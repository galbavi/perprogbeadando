﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace TravelingSalesmanGA
{
    class TownFitness
    {
        List<Point> towns;
        float fitness;

        public List<Point> Towns { get => towns; set => towns = value; }
        public float Fitness { get => fitness; set => fitness = value; }
    }
}
