﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.IO;
using System.Collections.Concurrent;
using System.Threading;

namespace TravelingSalesmanGA
{
    public class TravelingSalesmanSolverSequential
    {
        private static Random rnd = new Random();
        private StreamWriter streamWriter;
        private List<Point> listOfPoints;
        private List<TownFitness> population;
        private TownFitness p_best;
        private int max_population;
        private int iteration;
        private float last_fitness;
        private int condition_cnt;

        private object listLock = new object();


        public TravelingSalesmanSolverSequential(string filename)
        {
            population = new List<TownFitness>();
            LoadTowns(filename);
            max_population = 100;

            TaskScheduler taskScheduler = TaskScheduler.FromCurrentSynchronizationContext();
        }

        public List<Point> Result
        {
            get
            {
                return p_best.Towns;
            }
        }

        public void Start()
        {
            streamWriter = new StreamWriter("TS_GA.log");
            Optimize();
            streamWriter.Close();
        }

        private void Optimize()
        {
            last_fitness = -1;
            iteration = 0;

            InitializePopulation();
            Evaluation();

            p_best = SelectBest();
            SaveToLogFile(p_best);
            while (!StopCondition())
            {
                List<TownFitness> next_population = new List<TownFitness>();
                List<TownFitness> mating_pool = new List<TownFitness>();
                SelectParents(population, next_population, mating_pool);
                int diff = population.Count - next_population.Count;
                int count = diff / Environment.ProcessorCount;

                while (next_population.Count < population.Count)
                {
                    List<TownFitness> selected_parents = Selection(mating_pool);
                    TownFitness c = Crossover(selected_parents);
                    c = Mutate(c);
                    next_population.Add(c);
                }

                population = next_population;
                Evaluation();
                p_best = SelectBest();
                SaveToLogFile(p_best);
            }
        }

        private void LoadTowns(string filename)
        {
            listOfPoints = new List<Point>();
            string[] lines = File.ReadAllLines(filename);
            foreach (string line in lines)
            {
                Point p = new Point(double.Parse(line.Split('\t')[0]), double.Parse(line.Split('\t')[1]));
                listOfPoints.Add(p);
            }
        }

        private void InitializePopulation()
        {
            for (int i = 0; i < max_population; i++)
            {
                TownFitness new_element = MakeNewElement();
                population.Add(new_element);
            }
        }

        private TownFitness MakeNewElement()
        {
            TownFitness new_element = new TownFitness
            {
                Towns = listOfPoints.Select(item => item).ToList()
            };
            int n = listOfPoints.Count;
            while (n > 1)
            {
                n--;
                int k = rnd.Next(n + 1);
                Point value = new_element.Towns[n];
                new_element.Towns[n] = new_element.Towns[k];
                new_element.Towns[k] = value;
            }
            return new_element;
        }

        private void Evaluation()
        {
            for (int i = 0; i < max_population; i++)
            {
                population[i].Fitness = Fitness(population[i].Towns);
            }
        }

        private float Fitness(List<Point> towns)
        {
            float sum_length = 0;
            for (int i = 0; i < towns.Count - 1; i++)
            {
                Point t1 = towns[i];
                Point t2 = towns[i + 1];
                sum_length += (float)Math.Sqrt(Math.Pow(t1.X - t2.X, 2) + Math.Pow(t1.Y - t2.Y, 2));
            }
            return sum_length;
        }

        private TownFitness SelectBest()
        {
            float fit = population.Min(e => e.Fitness);
            return population.Find(item => item.Fitness == fit);
        }

        private bool StopCondition()
        {
            //    iteration++;
            //    float actual_fitness = p_best.Fitness;
            //    if (last_fitness == actual_fitness)
            //        condition_cnt++;
            //    else
            //        condition_cnt = 0;
            //    last_fitness = actual_fitness;
            //    return condition_cnt == 100;

            return iteration++ == 10000;
        }

        private void SelectParents(List<TownFitness> population, List<TownFitness> next_population, List<TownFitness> mating_pool)
        {
            population = population.OrderBy(item => item.Fitness).ToList();
            mating_pool.AddRange(population.GetRange(0, (population.Count / 4) - 1));
        }

        private List<TownFitness> Selection(List<TownFitness> mating_pool)
        {
            List<TownFitness> selected_ps = new List<TownFitness>();
            for (int i = 0; i < 2; i++)
            {
                selected_ps.Add(mating_pool[rnd.Next(0, mating_pool.Count)]);
            }
            return selected_ps;
        }

        private TownFitness Crossover(List<TownFitness> selected_parents)
        {
            int startindex = rnd.Next(0, listOfPoints.Count);
            int lastindex = rnd.Next(startindex, listOfPoints.Count);
            TownFitness c = new TownFitness
            {
                Towns = selected_parents[0].Towns.Select(item => item).ToList()
            };
            int cindex = 0;
            for (int i = 0; i < selected_parents[1].Towns.Count; i++)
            {
                int j = startindex;
                while (j <= lastindex && (selected_parents[1].Towns[i] != selected_parents[0].Towns[j]))
                {
                    j++;
                }
                if (j > lastindex)
                {
                    if (cindex == startindex)
                    {
                        cindex = lastindex + 1;
                    }
                    if (cindex == c.Towns.Count)
                        break;

                    c.Towns[cindex] = selected_parents[1].Towns[i];
                    cindex++;
                }
            }
            
            return c;
        }

        private TownFitness Mutate(TownFitness c)
        {
            if (rnd.Next(0, 100) < 80)
            {
                int index0 = rnd.Next(0, c.Towns.Count);
                int index1 = rnd.Next(0, c.Towns.Count);
                Point value = c.Towns[index0];
                c.Towns[index0] = c.Towns[index1];
                c.Towns[index1] = value;
            }
            return c;
        }
        
        private void SaveToLogFile(TownFitness townFitness)
        {
            streamWriter.WriteLine("Clear");
            streamWriter.WriteLine("Iteration\t" + iteration);
            streamWriter.WriteLine("Fitness\t" + townFitness.Fitness);
            for (int i = 0; i < townFitness.Towns.Count; i++)
            {
                streamWriter.WriteLine("Point\t" + townFitness.Towns[i].X + "\t" + townFitness.Towns[i].Y + "\tBlue");
            }

            for (int i = 0; i < townFitness.Towns.Count; i++)
            {
                streamWriter.WriteLine("Arrow\t" + townFitness.Towns[i].X + "\t" + townFitness.Towns[i].Y + 
                    "\t" + townFitness.Towns[(i + 1) % townFitness.Towns.Count].X + "\t" + townFitness.Towns[(i + 1) % townFitness.Towns.Count].Y + "\t" + "red");
            }
        }
    }
}
