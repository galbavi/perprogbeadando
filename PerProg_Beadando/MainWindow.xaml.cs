﻿using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Threading;
using TravelingSalesmanGA;

namespace PerProg_Beadando
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        ViewModel VM;
        private DispatcherTimer timer;

        public MainWindow()
        {
            InitializeComponent();
            VM = new ViewModel();
            DataContext = VM;
            Loaded += MainWindow_Loaded;
        }

        private void MainWindow_Loaded(object sender, RoutedEventArgs e)
        {
            timer = new DispatcherTimer();
            timer.Tick += Timer_Tick;
        }

        private void Timer_Tick(object sender, EventArgs e)
        {
            VM.Time = (DateTime.Now - VM.StartTime).ToString();
        }

        private async void StartParallelButton_Click(object sender, RoutedEventArgs e)
        {
            ((Button)sender).IsEnabled = false;
            TaskFactory taskFactory = new TaskFactory();
            VM.StartTime = DateTime.Now;
            timer.Start();
            try
            {
                TravelingSalesmanSolver travelingSalesmanSolver = new TravelingSalesmanSolver(VM.FilePath);
                await taskFactory.StartNew(() => travelingSalesmanSolver.Start());
                timer.Stop();
                VM.Time = (DateTime.Now - VM.StartTime).ToString();
                Process.Start(@"..\..\..\LogViewer.exe");
            }
            catch(ArgumentNullException)
            {
                timer.Stop();
                MessageBox.Show("Nincs fájl kiválasztva!", "Error", MessageBoxButton.OK, MessageBoxImage.Error, MessageBoxResult.OK);
            }
            catch (FormatException)
            {
                timer.Stop();
                MessageBox.Show("Nincs megfelelő a fájl!", "Error", MessageBoxButton.OK, MessageBoxImage.Error, MessageBoxResult.OK);
            }
            ((Button)sender).IsEnabled = true;
        }

        private void FileChoose_Click(object sender, RoutedEventArgs e)
        {
            OpenFileDialog openFileDialog = new OpenFileDialog();
            if(openFileDialog.ShowDialog() == true)
            {
                VM.FilePath = openFileDialog.FileName;
            }
        }

        private async void StartSequentialButton_Click(object sender, RoutedEventArgs e)
        {
            ((Button)sender).IsEnabled = false;
            TaskFactory taskFactory = new TaskFactory();
            VM.StartTime = DateTime.Now;
            timer.Start();
            try
            {
                TravelingSalesmanSolverSequential travelingSalesmanSolverSequential  = new TravelingSalesmanSolverSequential(VM.FilePath);
                await taskFactory.StartNew(() => travelingSalesmanSolverSequential.Start());
                timer.Stop();
                VM.Time = (DateTime.Now - VM.StartTime).ToString();
                Process.Start(@"..\..\..\LogViewer.exe");
            }
            catch (ArgumentNullException)
            {
                timer.Stop();
                MessageBox.Show("Nincs fájl kiválasztva!", "Error", MessageBoxButton.OK, MessageBoxImage.Error, MessageBoxResult.OK);
            }
            catch (FormatException)
            {
                timer.Stop();
                MessageBox.Show("Nincs megfelelő a fájl!", "Error", MessageBoxButton.OK, MessageBoxImage.Error, MessageBoxResult.OK);
            }
            ((Button)sender).IsEnabled = true;
        }
    }
}
