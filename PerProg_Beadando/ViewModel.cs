﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Threading;

namespace PerProg_Beadando
{
    class ViewModel : Bindable
    {
        private string filePath;
        private string time;
        private DateTime startTime;


        public string FilePath
        {
            get
            {
                return filePath;
            }
            set
            {
                filePath = value;
                OnPropertyChanged();
            }
        }

        public string Time
        {
            get
            {
                return time;
            }
            set
            {
                time = value;
                OnPropertyChanged();
            }
        }

        public DateTime StartTime { get => startTime; set => startTime = value; }
    }
}
